package com.b2w.desafio.commons;

import com.b2w.desafio.model.Planet;
import lombok.Data;

import javax.validation.Valid;

/**
 * Created by Ricardo on 01/07/2019.
 */
@Data
public class PlanetRequestBody {

    @Valid
    private Planet planet;

    public Planet getPlanet() {
        return planet;
    }

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }

}
