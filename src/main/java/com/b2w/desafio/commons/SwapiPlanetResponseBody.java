package com.b2w.desafio.commons;

import com.b2w.desafio.model.SwapiPlanet;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Created by Ricardo on 01/07/2019.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class SwapiPlanetResponseBody {

    public Integer count;
    public Object next;
    public Object previous;

    public List<SwapiPlanet> results;

    public SwapiPlanetResponseBody() {
    }

    public List<SwapiPlanet> getResults() {
        return results;
    }

    public void setResults(List<SwapiPlanet> results) {
        this.results = results;
    }

}
