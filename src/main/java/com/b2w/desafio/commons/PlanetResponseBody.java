package com.b2w.desafio.commons;

import com.b2w.desafio.model.Planet;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.util.List;

/**
 * Created by Ricardo on 01/07/2019.
 */
@JsonInclude(Include.NON_NULL)
@Data
public class PlanetResponseBody {

    private String description;

    private List<Planet> planets;

    private Planet planet;

    public PlanetResponseBody() {}

    public PlanetResponseBody(List<Planet> planets) {
        this.planets = planets;
    }

    public PlanetResponseBody(Planet planet) {
        this.planet = planet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    public Planet getPlanet() {
        return planet;
    }

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }
}
