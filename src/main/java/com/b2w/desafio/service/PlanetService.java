package com.b2w.desafio.service;

import com.b2w.desafio.model.Planet;
import com.b2w.desafio.model.SwapiPlanet;
import com.b2w.desafio.repository.PlanetRepository;
import com.b2w.desafio.validation.SwapiValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ricardo on 01/07/2019.
 */
@Slf4j
@Service
public class PlanetService {

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private SwapiPlanetService swapiPlanetService;

    @Autowired
    private CounterService CounterService;

    /**
     * Busca de todos os planetas cadastrados     *
     * @return List<Planet> planetas
     */
    public List<Planet> findAll() {
        return planetRepository.findAll();
    }

    /**
     * Busca de planeta pelo ID     *
     * @param id
     * @return Optional<Planet> planeta
     */
    public Optional<Planet> findById(String id) {
        return Optional.ofNullable(
                planetRepository.findById(
                        Long.parseLong(id)
                )
        );
    }

    /**
     * Busca de planeta pelo Nome     *
     * @param nome
     * @return
     */
    public Optional<Planet> findByNome(String nome) {
        return Optional.ofNullable(planetRepository.findByNome(nome));
    }

    /**
     * Adicionar um planeta (com nome, clima e terreno)
     * @param planetRequest
     * @return Planet planeta
     */
    public Planet create(Planet planetRequest) throws SwapiValidationException {

        Planet planet = new Planet();
        planet.setNome(planetRequest.getNome());
        planet.setClima(planetRequest.getClima());
        planet.setTerreno(planetRequest.getTerreno());
        planet.setFilmesTotal(this.aparicoesEmFilmesTotal(planetRequest.getNome()));
        planet.setId(CounterService.getNextSequence("Counter"));

        return planetRepository.save(planet);
    }

    /**
     * Verifica se existe algum Planet criado com o nome passado pelo parametro
     * @param planetaNome
     * @return boolean
     */
    public boolean isThereAPlanetNamed(String planetaNome) {
        Optional<Planet> planetaOptional = this.findByNome(planetaNome);

        if(planetaOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deleta no banco de dados o Planet recebido por parâmetro
     * @param planet
     */
    public void delete(Planet planet) {
        planetRepository.delete(planet);
    }

    /**
     * Retorna o Total de aparicoes do planeta nos filmes
     * @param nome
     * @return
     * @throws SwapiValidationException
     */
    public Integer aparicoesEmFilmesTotal(String nome) throws SwapiValidationException {

        SwapiPlanet swapiPlanet = swapiPlanetService.getPlanetInSwapiByName(nome);

        return swapiPlanet.getFilms().size();
    }

    /**
     * Deleta TODOS os planetas no banco de dados
     */
    public void deleteAll() {
        planetRepository.deleteAll();
    }
}
