package com.b2w.desafio.repository;

import com.b2w.desafio.model.Counter;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Ricardo on 01/07/2019.
 */
public interface CounterRepository extends MongoRepository<Counter, String> {

}
