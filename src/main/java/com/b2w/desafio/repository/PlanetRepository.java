package com.b2w.desafio.repository;

import com.b2w.desafio.model.Planet;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Ricardo on 01/07/2019.
 */
public interface PlanetRepository extends MongoRepository<Planet, String> {

    Planet findByNome(String nome);

    Planet findById(long id);

}
