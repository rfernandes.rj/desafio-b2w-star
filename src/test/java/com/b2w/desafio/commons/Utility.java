package com.b2w.desafio.commons;

import com.b2w.desafio.model.Planet;
import com.b2w.desafio.model.SwapiPlanet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricardo on 01/07/2019.
 */
@Service
public class Utility {

    public static List<Planet> getPlanetasDefault() {

        List<Planet> planets = new ArrayList<>();

        Planet planet1 = new Planet();
        planet1.setNome("Tatooine");
        planet1.setClima("arid");
        planet1.setTerreno("desert");

        Planet planet2 = new Planet();
        planet2.setNome("Alderaan");
        planet2.setClima("temperate");
        planet2.setTerreno("grasslands, mountains");

        Planet planet3 = new Planet();
        planet3.setNome("Yavin IV");
        planet3.setClima("temperate, tropical");
        planet3.setTerreno("jungle, rainforests");

        planets.add(planet1);
        planets.add(planet2);
        planets.add(planet3);

        return planets;
    }

    public static String getJsonCadastroPlaneta(Planet planet) {

        String jsonPlaneta = "{  \n" +
                "   \"planet\":{  \n" +
                "      \"nome\":\""+ planet.getNome() +"\",\n" +
                "      \"clima\": \""+ planet.getClima() +"\",\n" +
                "      \"terreno\": \""+ planet.getTerreno() +"\"\n" +
                "   }\n" +
                "}";

        return jsonPlaneta;
    }

    public static String getJsonCadastroPlanetaBadRequest(Planet planet) {

        String jsonPlaneta = "{  \n" +
                "   \"planet\":{  \n" +
                "      \"nome\":\""+ planet.getNome() +"\",\n" +
                "      \"climas\": \""+ planet.getClima() +"\",\n" +
                "      \"terreno\": \""+ planet.getTerreno() +"\"\n" +
                "   }\n" +
                "}";

        return jsonPlaneta;
    }

    public static SwapiPlanet getSwapiPlanetDeUmPlaneta(Planet planet) {

        List<Object> filmes = new ArrayList<>();
        filmes.add("filme 1");
        filmes.add("filme 2");
        filmes.add("filme 3");

        SwapiPlanet swapiPlanet = new SwapiPlanet();
        swapiPlanet.setName(planet.getNome());
        swapiPlanet.setClimate(planet.getClima());
        swapiPlanet.setTerrain(planet.getTerreno());
        swapiPlanet.setFilms(filmes);

        return swapiPlanet;
    }
}
